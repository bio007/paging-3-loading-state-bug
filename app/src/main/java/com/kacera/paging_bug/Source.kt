package com.kacera.paging_bug

import androidx.paging.PagingSource
import androidx.paging.PagingState

class Source : PagingSource<Int, String>() {
    override fun getRefreshKey(state: PagingState<Int, String>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, String> {
        return LoadResult.Page(emptyList(), null, null)
    }
}