package com.kacera.paging_bug

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn

class MainActivityViewModel : ViewModel() {

    private val pagingSource: Source = Source()

    val flow = Pager(PagingConfig(0)) { pagingSource }.flow.cachedIn(viewModelScope)
}