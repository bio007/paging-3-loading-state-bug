package com.kacera.paging_bug

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.collectLatest

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv = findViewById<RecyclerView>(R.id.recycler_view)

        rv.adapter = Adapter().apply {
            addLoadStateListener {
                Log.d("Paging State", "on change -> $it ")
            }
            lifecycleScope.launchWhenStarted {
                viewModel.flow.collectLatest { pagingData ->
                    this@apply.submitData(pagingData)
                }
            }
        }
    }
}