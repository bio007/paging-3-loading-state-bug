## Sample project showing incorrect loading states from paging source

When observing load states from `PagingSource` via `PagingDataAdapter`'s `addLoadStateListener` user can see inconsistent states if the source ends up empty.

Result from source's load function if the source is empty would look like this:

```kotlin
override suspend fun load(params: LoadParams<Int>): LoadResult<Int, String> {
    return LoadResult.Page(emptyList(), null, null)
}
```

**-> empty list is returned and `nulls` indicate there is nothing to prepend or append**


Expected sequence of states in such a case should naturally be:

1. refresh=NotLoading(endOfPaginationReached=false) // initial state
2. refresh=Loading(endOfPaginationReached=false)    // loading started but there is no result yet
3. refresh=NotLoading(endOfPaginationReached=true) // loading finished and there is nothing more to load

In reality it is this:

1. refresh=NotLoading(endOfPaginationReached=false) // initial state
2. refresh=Loading(endOfPaginationReached=false)    // loading started but there is no result yet
3. refresh=NotLoading(endOfPaginationReached=false) // loading finished and there is more to load **endOfPaginationReached=false**

Here are the complete logs for completeness:
```json
D/Paging State: on change -> CombinedLoadStates(refresh=NotLoading(endOfPaginationReached=false), prepend=NotLoading(endOfPaginationReached=false), append=NotLoading(endOfPaginationReached=false), source=LoadStates(refresh=NotLoading(endOfPaginationReached=false), prepend=NotLoading(endOfPaginationReached=false), append=NotLoading(endOfPaginationReached=false)), mediator=null)
```
```json
D/Paging State: on change -> CombinedLoadStates(refresh=Loading(endOfPaginationReached=false), prepend=NotLoading(endOfPaginationReached=false), append=NotLoading(endOfPaginationReached=false), source=LoadStates(refresh=Loading(endOfPaginationReached=false), prepend=NotLoading(endOfPaginationReached=false), append=NotLoading(endOfPaginationReached=false)), mediator=null)
```
```json
D/Paging State: on change -> CombinedLoadStates(refresh=NotLoading(endOfPaginationReached=false), prepend=NotLoading(endOfPaginationReached=true), append=NotLoading(endOfPaginationReached=true), source=LoadStates(refresh=NotLoading(endOfPaginationReached=false), prepend=NotLoading(endOfPaginationReached=true), append=NotLoading(endOfPaginationReached=true)), mediator=null)
```